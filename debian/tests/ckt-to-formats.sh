#!/bin/bash

set -e
echo "Converting example file to different formats..."
/usr/bin/pycirkuit -t -p -f /usr/share/doc/pycirkuit/examples/*.ckt --destination /tmp > /dev/null
echo "Done."
