#!/bin/sh
set -e

# Check if the help menu is displayed.
echo "Test 1: checking output..."
if pycirkuit --help | grep -q "Options:"; then
        echo "Done."
fi
